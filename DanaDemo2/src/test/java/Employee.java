import demo.EmployeeRequest;
import demo.EmployeeResponse;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;


public class Employee {
    @Test
    public void getEmployee(){
        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log() //munculin semua response
                .all() //munculin semua request
                .header("Content-type","application/json")
                .get("/v1/employees");

        //print response
        //response.getBody().prettyPrint();
        //System.out.println(response.getStatusCode());
        //check responsenya sesuai gak
        Assert.assertEquals(200,response.getStatusCode());
        //check response time dibawah ekspektasi ga
        Assert.assertThat("Lama cuy", response.getTime(), Matchers.lessThan(3000L));
        //tambahin L karena less than expectnya Long bukan Integer

        //SinglePath response Body
        Assert.assertEquals("success",response.path("status"));
        Assert.assertEquals("Tiger Nixon",response.path("data[0].employee_name"));

        EmployeeResponse employeeResponse = response.as(EmployeeResponse.class);
        System.out.println(employeeResponse.getData().get(0).getEmployeeName());


    }

    @Test
    public void createEmployee() {


        EmployeeRequest employeeRequest = new EmployeeRequest();
        employeeRequest.setName("namasaya");
        employeeRequest.setAge("19");
        employeeRequest.setSalary("10000");

        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log() //munculin semua response
                .all() //munculin semua request
                .header("Content-type","application/json")
                .body(employeeRequest)
                .post("/v1/create");

        response.getBody().prettyPrint();

    }
}
